package com.psaha.cpf.dao.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import com.psaha.cpf.dao.SupplierMasterMapper;
import com.psaha.cpf.dao.util.SPDAOManager;
import com.psaha.cpf.data.SupplierMaster;

public class SupplierMasterMapperImpl implements SupplierMasterMapper{

	@Override
	public void insertSupplier(SupplierMaster supplier) {
		SqlSession sqlSession = SPDAOManager.getSqlSessionFactory().openSession();
		try{
			SupplierMasterMapper supplierMapper = sqlSession.getMapper(SupplierMasterMapper.class);
			supplierMapper.insertSupplier(supplier);
			sqlSession.commit();
		}finally{
			sqlSession.close();
		}

	}

	@Override
	public SupplierMaster getSupplierById(String supplierId) {
		SqlSession sqlSession = SPDAOManager.getSqlSessionFactory().openSession();
		try{
			SupplierMasterMapper supplierMapper = sqlSession.getMapper(SupplierMasterMapper.class);
			return supplierMapper.getSupplierById(supplierId);
		}finally{
			sqlSession.close();
		}
	}

	@Override
	public List<SupplierMaster> getAllSuppliers() {
		SqlSession sqlSession = SPDAOManager.getSqlSessionFactory().openSession();
		try{
			SupplierMasterMapper supplierMapper = sqlSession.getMapper(SupplierMasterMapper.class);
			return supplierMapper.getAllSuppliers();
		}finally{
			sqlSession.close();
		}
	}

	@Override
	public void updateSupplier(SupplierMaster supplier) {
		SqlSession sqlSession = SPDAOManager.getSqlSessionFactory().openSession();
		try{
			SupplierMasterMapper supplierMapper = sqlSession.getMapper(SupplierMasterMapper.class);
			supplierMapper.updateSupplier(supplier);
			sqlSession.commit();
		}finally{
			sqlSession.close();
		}
	}

	@Override
	public void deleteSupplier(Integer supplierId) {
		SqlSession sqlSession = SPDAOManager.getSqlSessionFactory().openSession();
		try{
			SupplierMasterMapper supplierMapper = sqlSession.getMapper(SupplierMasterMapper.class);
			supplierMapper.deleteSupplier(supplierId);
			sqlSession.commit();
		}finally{
			sqlSession.close();
		}

	}

}
