# Core Persistence Framework(CPF) with Oracle Database #

This framework consists of the steps are necessary to get and store the data into the Oracle database. I have used MyBatis as 
Persistence Framework. MyBatis is an open source project from the Apache Software Foundation.

* It is a Data Mapper framework.
* The primary goal of MyBatis is to do 80% of the work with 20% of coding.
* It is not an Object-Relational Mapping (ORM) tool.

## Working with MyBatis ##
All database access is done in the Data Mapper XML files. DBA, DB Developer can easily access SQL or Stored Procedure calls
without coding a single line of JDBC. 

* DBA and DB Developer can easily try out new SQL or Stored Procedures as long as it does not modify the result.
* Data Access Object (DAO) is the layer where application business logic and data access logic is separated.
* Since MyBatis was mean to do 80% of the JDBC coding with only 20% of the code.

* There are certain things MyBatis does not support. Such as LOB access in this case we will have to code JDBC around it with Objects.

### Prerequisites ###

* Oracle Database (Oracle XE 12C has been used)
* Sample Data in Oracle (Craeted a SUPPLIER_MASTER table in Oracle database. Uploaded with 1000 dummy data)
* (IDE https://www.eclipse.org/ide/) (Eclipse IDE has been used with Maven plugin)
* JDK 1.8 or Open JDK 8
* Download MyBatis from http://blog.mybatis.org/ version 3.5.0 (20 Jan 2019)
* Oracle JDBC driver, located at $ORACLE_HOME/jdbc/lib, or jar can be download from Oracle website (https://www.oracle.com/technetwork/apps-tech/jdbc-112010-090769.html)
* Oracle 10g - ojdbc14.jar or, Oracle 11g  - ojdbc5.jar or ojdbc6.jar

## CPF Architecture ##

![CPF Architecture](img/cpf_arch.png)


## Implementation ##
_1. SqlMapConfig.xml_

* SQL Map XML files. One XML file per object.
  Supplier.xml
  Item.xml
  
* SqlMapConfig.xml contains the DB connection information.

* It also contains a list of all the SQL Map XML files.

_2. Java Bean_

* Java Bean is an object that is used to pass around data that is a package of other objects and primitive types

_3. SQL Map_

* We have the Java Bean to hold employee information. It is time to create the SQL to Java Object Map using the SQL Map file.

* SQL Map Supplier.xml (Auto-generated) will have SELECT, INSERT, UPDATE, DELETE SQL queries.

_4. JAVA code to Run_

* We have the Java Bean Supplier and the necessary SQL Map. It is time to write a small Java App to access DB through MyBatis SQL Map

_5. DAOManager.java_

- Create an DAO interface class and create an DAO implementation class.

DAO Interface is like a contract between Java classes.

* DAO Interface is abstract.
* Interface only contains method signatures.
* Interface needs to be implemented

There can be as many DAO implementation as we want. This JAVA file can generated automatically. We can have another implementation that
uses straight JDBC. We can have another implementation that uses Hibernate.

_6. Java Test Program_


## Recap ##

* Created SqlMapConfig.xml
* Created Java bean Supplier.java
* Created the Supplier.xml SQL Map XML file.
* Create the Java test application AppTest.java to access the DB


## Advanced Topic ##
Oracle Stored Procedure :: Oracle SP : get

SQL Map Employee XML
* Before we could start implementing the DAO, we also need the SQL Map XML file that will map Stored Procedures to Java Object.

SQL Map Supplier XML : add Example

```<procedure id="add" parameterMap="addMap" >
begin supplier_util.add(?,?,?,?,?,?,?);
end;
</procedure>```


## Debug in MyBatis ##

Debuging is easy in MyBatis. MyBatis shows the exceptions and it is obvious as to why things did not work as intended. 
This means we can check by seeing a SQLException that is visible in the the exact ORA- error that happened.