package com.psaha.cpf.test;

import java.util.List;

import com.psaha.cpf.dao.SupplierMasterMapper;
import com.psaha.cpf.dao.impl.SupplierMasterMapperImpl;
import com.psaha.cpf.data.SupplierMaster;

public class AppTest {

	public static void main(String[] args) {
		SupplierMasterMapper oMapper = new SupplierMasterMapperImpl();
		List<SupplierMaster> oSupplierMasters = oMapper.getAllSuppliers();
		
		for (SupplierMaster supplier : oSupplierMasters) {
			System.out.println(supplier.getSupplierName() + "||" + supplier.getCountry() + "||" + supplier.getBankName() + "||" + supplier.getProductionSite());
		}
	}
}
